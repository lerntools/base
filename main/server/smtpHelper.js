//External libs
const nodemailer=require('nodemailer');
const debug=require('debug')('helpers');
const path=require('path');

//Internal libs
const config=__config;
const consts=require('../../consts.js');
const Templates = require('./templates/templates');

//Database schemas
const User=require('./modelUser.js');

//Check if test is a valid email address
exports.isValidMail=function(test) {
	if (!test) return false;
	return test.match(consts.REGEX_EMAIL);
}

/*
 Send info mail to registered user
 to: either email-address or registered user
 subject: subject email
 html: text email
 */
exports.sendInfoMail=function(to, subject, html) {
	if (!to) return;
	if (!subject) subject="no subject";
	if (!html) html="";
	var from=config.INFO_MAIL;
	//valid email address?
	if (to.match(consts.REGEX_EMAIL)) {
		return sendMail(from, to, subject, html);
	}
	//regsitered user?
	User.findOne({ login: to }, function (err, user) {
		if (err) debug(err);
		if (user) {
			sendInfoMailToUser(user, subject, html);
		}
		else debug('Couldn\'t send email to: '+to)
	})
}

exports.sendMailTplToUser = async function (user, tplId, options) {
	const mail = await prepareMail(tplId, user.lang, options);
	sendMailToUserAddress(user.email, mail);
}

exports.sendMailTplToAddress = async function (recipientAddr, tplId, options, language=null) {
	// We use the default instance language
	if (!language) {
		language = config.DEFAULT_LOCALE;
	}
	const mail = await prepareMail(tplId, language, options);
	sendMailToUserAddress(recipientAddr, mail);
}

async function prepareMail(tplId, language, options) {
	// Mail templates always start with mails::!
	if (!tplId.startsWith('mails::')) {
		tplId = 'mails::' + tplId;
	}
	// always provide the server side configuration.
	// Only specific configs are allowed.
	options.config = {
		AUTHOR: config.AUTHOR,
		SITE_NAME: config.SITE_NAME,
		URL_HOST: config.URL_HOST,
		INFO_MAIL: config.INFO_MAIL,
		INFO_MAIL_APPS: config.INFO_MAIL_APPS,
		DEFAULT_LOCALE: config.DEFAULT_LOCALE,
		USER_ADMIN_MAIL: config.USER_ADMIN_MAIL,
		REGISTRATION: config.REGISTRATION,
		TAN_EXPIRES: config.TAN_EXPIRES,
		ACCOUNT_CLEANUP: config.ACCOUNT_CLEANUP
	};
	// and the base URL to the app.
	options.instanceUrl = config.URL_PROTO + path.join(config.URL_HOST,config.URL_PREFIX);
	const tpl = await Templates.getTemplate(tplId, language);
	return Templates.parseTemplate(tpl, options);
}

function sendInfoMailToUser(user, subject, html) {
	if (!user) return;
	if (!subject) subject="no subject";
	if (!html) html="";
	if (!user.email.match(consts.REGEX_EMAIL)) {
		return;
	}
	sendMail(config.INFO_MAIL, user.email, subject, html);
}
exports.sendInfoMailToUser = sendInfoMailToUser;

function sendMailToUserAddress(recipientAddr, mail) {
	if (!recipientAddr) return;
	if (!mail) return;
	if (!config.INFO_MAIL) return;
	
	sendMail(
		config.INFO_MAIL, recipientAddr, 
		mail.title, mail.content.html, 
		mail.content.text
	);
}

/*
 Use nodemailer for sending emails, internal function
 */
function sendMail(from, to, subject, html, text=undefined) {
	var transporter=nodemailer.createTransport(config.SMTP_CONF);
	// For HTML mails, ensure, there is a doctype and full html.
	// We may add here CSS code (via file data from css folder.)
	html = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>${subject}</title>
	</head>
	<body>
	${html}
	</body>
</html>`;
	var mailOptions={
		from: from,
		to: to,
		subject: subject,
		html: html
	};
	if (text) {
		mailOptions.text = text;
	}
	transporter.sendMail(mailOptions, (error, info)=> {
		if (error) debug(error);
		else debug('Sent email to: '+ to + ', ID: ' + info.messageId + " (response: " + info.response + ")");
	});
}
