% Title: Brukeradvarsel — Myk grense

Satt anbefalt grense på antall brukere ble overskredet på 
[{{config.SITE_NAME}}]({{instanceUrl}})-instansen.
