% Title: Kontoregistrering

En ny bruker har registrert seg på {{instanceUrl}}:

Navn: {{user.name}}
E-postadresse: {{user.email}}
Kommentar: {{user.comment}}
