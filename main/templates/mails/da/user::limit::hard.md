% Title: Advarsel fra brugeren — hård grænse

Den fastsatte hårde grænse for antallet af brugere blev nået på den 
instance [{{{config.SITE_NAME}}]]({{instanceUrl}}).
