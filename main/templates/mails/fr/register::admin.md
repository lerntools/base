% Title: Enregistrement du compte

Un·e nouvel·le utilisateur·trice s'est inscrit·e sur {{instanceUrl}} :

Nom : {{user.name}}
E-mail : {{user.email}}
Commentaire : {{user.comment}}
