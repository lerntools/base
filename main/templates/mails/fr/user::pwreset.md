% Title: Réinitialisation du mot de passe

Bonjour {{user.name}},

un nouveau mot de passe a été demandé pour ton compte sur [{{config.SITE_NAME}}]({{instanceUrl}}).

Cliquer sur le lien suivant pour confirmer et réinitialiser ton mot de passe :
[{{confirmUrl}}]({{confirmUrl}})

Si tu n'as pas demandé de nouveau mot de passe, ignore cet e-mail.
