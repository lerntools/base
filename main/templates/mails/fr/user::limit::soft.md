% Title: Alerte utilisateur·trice - Limite souple

La limite souple définie pour le nombre d'utilisateurs·trices a été atteinte sur l'adresse de l'instance
 [{{config.SITE_NAME}}]({{instanceUrl}}).
