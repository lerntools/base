% Title: Bienvenue sur {{config.SITE_NAME}}

Bonjour {{user.name}},

Bienvenue sur {{config.SITE_NAME}}. Ton compte est prêt.

Va sur [{{instanceUrl}}]({{instanceUrl}}) et lance-toi !

Ton équipe Lerntools