% Title: Nutzerwarnung - Hart-Limit

Das gesetzte Hart-Limit für die Anzahl Nutzer wurde auf der 
Instanz [{{config.SITE_NAME}}]({{instanceUrl}}) erreicht.
