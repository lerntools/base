% Title: Konto gelöscht

Hallo {{user.name}},

Dein Konto mit allen Nutzerdaten wurde nun von [{{config.SITE_NAME}}]({{instanceUrl}}) gelöscht.

Dieser Prozess kann nicht rückgängig gemacht werden. Du kannst jederzeit ein neues Konto erstellen.

Dein Lerntools-Team