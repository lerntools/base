% Title: Account cancellato

Ciao {{user.name}},

L'account dell'utente con tutti i dati è stato cancellato da [{{config.SITE_NAME}}]({{instanceUrl}}).

Questo processo non può essere invertito. È possibile creare un nuovo account in qualsiasi momento.

Il vostro team di strumenti di apprendimento