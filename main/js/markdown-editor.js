export default {
    toolbox: class {
        constructor(textarea, callback=undefined) {
            this.textarea = textarea;
            this.callback = callback;
        };

        execute(btnType) {
            switch(btnType) {
                case 'bold': 
                    this.bold();
                    break;
                case 'italic': 
                    this.italic();
                    break;
                case 'underline': 
                    this.underline();
                    break;
                case 'strikethrough': 
                    this.strikethrough();
                    break;
                case 'quote': 
                    this.quote();
                    break;
                case 'h1': 
                    this.headline(1);
                    break;
                case 'h2': 
                    this.headline(2);
                    break;
                case 'h3': 
                    this.headline(3);
                    break;
                case 'h4': 
                    this.headline(4);
                    break;
                case 'list': 
                    this.list();
                    break;
                case 'list-numbered': 
                    this.listNumbered();
                    break;
                case 'insert-link':
                    this.link();
                    break;
                case 'insert-image':
                    this.image();
                    break;
                case 'insert-code':
                    this.code();
                    break;
            }
        };

        bold() {
            this._format('**');
        }; 
        italic() {
            this._format('*');
        };
        underline() {
            this._format('_');
        };
        strikethrough() {
            this._format('~~');
        };
        quote() {
            this._prependLine('> ');
        };
        headline(num = 1) {
            this._prependLine('#'.repeat(num) + ' ');
        };
        list() {
            this._prependLine('- ');
        }
        listNumbered() {
            this._prependLine('1. ');
        }
        code() {
            this._format('`');
        }
        link(prepend='') {
            var startPos = this.textarea.selectionStart;
            var endPos = this.textarea.selectionEnd;
            var data = this.textarea.value.substring(startPos, endPos);
            var url = 'https://';
            var validUrl = this.isUrl(data);
            if (validUrl) {
                url = data.replace('(', '\\(').replace(')', '\\)');
            } else if (data.length <= 0) {
                data = 'Link';
            }
            data = data.replace('[', '\\[').replace(']', '\\]');
            var wrapper = prepend + '[' + data + ']' + '(' + url + ')';
            this.textarea.value = 
                this.textarea.value.substring(0, startPos) + 
                wrapper + 
                this.textarea.value.substring(endPos);

            if (startPos == endPos) {
                // Position at url input
                startPos = startPos + prepend.length + 3 + data.length + url.length;
                endPos = startPos;
            } else if (validUrl) {
                // Select data
                startPos = startPos + prepend.length + 1;
                endPos = startPos + data.length;
            } else {
                // Select URL
                startPos = startPos + prepend.length + 1 + data.length + 2;
                endPos = startPos + url.length;
            }
            // set cursor
            this.textarea.focus();
            this.textarea.selectionStart = startPos;
            this.textarea.selectionEnd = endPos;

            // callback
            if (this.callback !== undefined) {
                this.callback(this.textarea.value);
            }
        }
        isUrl(url) {
            const reg = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;
            return reg.test(url);
        }
        image() {
            this.link('!');
        }

        _format(wrapper) {
            var startPos = this.textarea.selectionStart;
            var endPos = this.textarea.selectionEnd;
            this.textarea.value = 
                this.textarea.value.substring(0, startPos) + 
                wrapper + 
                this.textarea.value.substring(startPos, endPos) + 
                wrapper + 
                this.textarea.value.substring(endPos);
                
            // set cursor
            this.textarea.focus();
            this.textarea.selectionStart = startPos + wrapper.length;
            this.textarea.selectionEnd = endPos + wrapper.length;

            // callback
            if (this.callback !== undefined) {
                this.callback(this.textarea.value);
            }
        };

        _prependLine(wrapper) {
            var startPos = this.textarea.selectionStart;
            var endPos = this.textarea.selectionEnd;
            var positionNewLine = this.textarea.value.substring(0, startPos).lastIndexOf('\n');
            var toAddStart = 0;
            var toAddEnd = 0;
            var selectionContent = this.textarea.value.substring(
                startPos, endPos
            );
            var dataAfter = this.textarea.value.substring(endPos);
            var prependData = '';
            var appendData = '';

            if (dataAfter.length > 0) {
                dataAfter = '\n\n';
            } else {
                dataAfter = '\n';
            }

            if (positionNewLine < 0) {
                // check if there is any content in front.
                if (this.textarea.value.substring(0, startPos).trim().length > 0) {
                    prependData = prependData += '\n';
                }
            }
            if (positionNewLine >= 0) {
                // check if there is any content in front.
                if (this.textarea.value.substring(positionNewLine, startPos).trim().length > 0) {
                    prependData = prependData += '\n';
                }
            }
            
            this.textarea.value = 
                this.textarea.value.substring(0, startPos) + 
                    prependData + wrapper + selectionContent + 
                    dataAfter + 
                    this.textarea.value.substring(endPos);
            toAddStart = toAddStart + prependData.length + 
                wrapper.length;
            toAddEnd = toAddStart + appendData.length;

            this.textarea.focus();
            this.textarea.selectionStart = startPos + toAddStart;
            this.textarea.selectionEnd = endPos + toAddEnd;

            // callback
            if (this.callback !== undefined) {
                this.callback(this.textarea.value);
            }
        }
    }
}