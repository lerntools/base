import Vue from 'vue'

let design = 'light'
const mql = window.matchMedia('(prefers-color-scheme: dark)')
if (mql.matches) {
  design = 'dark'
}

const state = Vue.observable({
  user: {
    login: '',
    roles: [],
    name: '',
    email: '',
    accessToken: '',
    refreshToken: '',
    hasRole: function (role) {
      return this.roles && this.roles.indexOf(role) >= 0
    },
    clear: function () {
      this.login = ''
      this.roles = []
      this.name = ''
      this.email = ''
      this.accessToken = ''
      this.refreshToken = ''
    }
  },
  allModules: [],
  design,
  lang: 'de',
  subnav: {}
})

export default {
  user: state.user,
  design: state.design,
  lang: state.lang,
  allModules: state.allModules,
  subnav: state.subnav
}
