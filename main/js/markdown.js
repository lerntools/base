import MarkdownIt from 'markdown-it';
import MarkdownItAttrs from 'markdown-it-attrs';
import MarkdownItTexmath from 'markdown-it-texmath';
import MarkdownItUnderline from 'markdown-it-underline';
import MarkdownItBlockquoteCite from 'markdown-it-blockquote-cite';
import MarkdownItTaskList from 'markdown-it-task-list'
import MarkdownItSup from 'markdown-it-sup'
import MarkdownItSub from 'markdown-it-sub'
import MarkdownItFootnote from 'markdown-it-footnote'
import MarkdownItDeflist from 'markdown-it-deflist'

//init markdown
var markdownIt=new MarkdownIt({breaks:true});
markdownIt.use(MarkdownItAttrs);
markdownIt.use(MarkdownItTexmath);
markdownIt.use(MarkdownItUnderline);
markdownIt.use(MarkdownItBlockquoteCite);
markdownIt.use(MarkdownItTaskList)
markdownIt.use(MarkdownItSub)
markdownIt.use(MarkdownItSup)
markdownIt.use(MarkdownItFootnote)
markdownIt.use(MarkdownItDeflist)

export default {
	/*
	convert markdown string to html using inline mode → result is not wrapped into an <p> paragraph
	line breaks are converted to <br>
	*/
	render: function(md) {
		try {
			const html = markdownIt.renderInline(md);
			return html
		}
		catch (e) {
			return ''
		}
	},
	renderFull: function(md) {
		try {
			const html = markdownIt.render(md);
			return html;
		}
		catch (e) {
			return ''
		}
	}
}
