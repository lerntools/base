# Gestion des utilisateurs·trices

Notes pour l'administration{.subtitle}


## Message à toutes et tous les utilisateurs·trices

Cette boîte de dialogue te permet d'envoyer un message à toutes et tous les utilisateurs·trices enregistré·es, en utilisant l'adresse e-mail enregistrée. Cette fonction ne doit être utilisée qu'avec précaution :

- Les utilisateurs·trices enregistré·es n'ont pas la possibilité de se « désinscrire » de ces informations et les messages inutiles agacent...
- Si le nombre d'utilisateurs·trices est élevé, les messages risquent d'être interprétés comme des spams par d'autres systèmes. Il n'y aurait donc plus aucun message provenant de ce serveur ; l'enregistrement et la réinitialisation du mot de passe ne seraient plus possibles.

## Liste des utilisateurs·trices

La liste affiche toutes et tous les utilisateurs·trices enregistré·es du système avec les détails suivants :

- Nom personnel (n'a pas d'autre signification dans le système)
- Nom d'utilisateur·trice (identifiant unique)
- Dernière connexion (date de la dernière connexion)
- Actif (les administrateurs·trices peuvent bloquer explicitement des utilisateurs·trices)
- Rôles (liste des rôles attribués, ceux-ci correspondent à des modules)

En cliquant sur l'en-tête de la colonne (par ex. « nom d'utilisateur·trice »), les entrées peuvent être triées par ordre croissant. Il est également possible de limiter la sélection au moyen d'une expression de recherche.

Les propriétés des utilisateurs·trices peuvent être modifiées à l'aide des icônes de la colonne Action. Pour des raisons de sécurité, il n'est toutefois pas possible de modifier son ou sa propre utilisateur·trice. Si cela s'avère nécessaire, il est possible de créer un·e deuxième utilisateur·trice avec le rôle d'admin et de modifier ainsi le premier.
