# Amministrazione dell'utente

Note per l'amministrazione{.subtitle}


## Messaggio per tutti gli utenti

Questa finestra di dialogo consente di inviare un messaggio a tutti gli utenti registrati, utilizzando l'indirizzo e-mail memorizzato in ciascun caso. Questa funzione deve essere utilizzata con cautela:

- Registrierte Benutzer haben keine Möglichkeit sich von diesen Informationen „auszuschreiben“ und unnötige Nachrichten nerven...
- Bei einer großen Anzahl an Benutzern besteht das Risiko, dass die Nachrichten von anderen Systemen als Spam interpretiert werden. Damit kämen keinerlei Mails von diesem Server mehr an, Registrierung und Passwort-Reset wären damit auch nicht mehr möglich.

## Elenco utenti

L'elenco mostra tutti gli utenti registrati del sistema con i seguenti dettagli:

- Persönlicher Name (hat systemintern keine weitere Bedeutung)
- Benutzername (eindeutige Benutzerkennung)
- Letztes Login (Datum der letzten Anmeldung)
- Aktiv (Administratoren können Benutzer explizit sperren)
- Rollen (Liste der zugewiesenen Rollen, diese entsprechen Modulen)

Facendo clic sull'intestazione della colonna (ad esempio, "nome utente"), le voci possono essere ordinate in ordine crescente ed è anche possibile limitare la selezione mediante un'espressione di ricerca.

Mittels der Icons in der Spalte Aktion können die Eigenschaften von Benutzern verändert werden. Es ist aus Sicherheitsgründen allerdings nicht möglich, den eigenen Benutzer zu bearbeiten. Sollte dies nötig sein, kann grundsätzlich ein zweiter Benutzer mit der Rolle admin angelegt werden und der erste damit bearbeitet werden.
