#!/usr/bin/env bash

# This script exists in order to execute the required steps before starting the Lerntools application
# Also it allows signals like SIGKILL to be passed down to the process

# Prerequesits
node bin/createConfigFromEnv.js
node bin/installDatabase.js

# Starting Lerntools
exec npm start
