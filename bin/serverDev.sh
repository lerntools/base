#!/bin/bash
# override environment variables only, if not set.
: ${NODE_ENV:=development}
: ${DEBUG:=abcd,ideas,survey,price,app,chat,share,scripts,main,helpers,memory,bookmark,translate,projector,templates}
: ${PORT:=8080}

export NODE_ENV
export DEBUG
export PORT
echo "Start server on port $PORT"
nodemon start
