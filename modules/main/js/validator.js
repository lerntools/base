export default {
	//validate input text angainst pattern
	validate: function(input,required,pattern) {
		try {
			if (!pattern) {console.log('Validator: missing pattern for input '+input); return false};
			if (input===0) input="0";
			if (!input && !required) return true;
			if (!input) return false;
			return input.toString().match(pattern);
		}
		catch (e) {
			return false;
		}
	}
}
