import CryptoJS from 'crypto-js';
import consts from '../../../consts.js';


/*
 Client sied symmetric encryption and decryption of messages.
 IMPORTANT: You have to catch errors when using this functions and handle errors in the view
 */
export default {
	encrypt: function(messageToencrypt='', secretkey=''){
		var encryptedMessage=CryptoJS.AES.encrypt(messageToencrypt, secretkey);
		return encryptedMessage.toString();
	},
	decrypt: function(encryptedMessage='', secretkey=''){
		var decryptedBytes=CryptoJS.AES.decrypt(encryptedMessage, secretkey);
		if (decryptedBytes.sigBytes<0) throw 'cant-decrypt';
		var decryptedMessage=decryptedBytes.toString(CryptoJS.enc.Utf8);
		return decryptedMessage;
	}
}
