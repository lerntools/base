//import katex from 'script(src='../../../node_modules/config.URL_PREFIX+'/lib/katex/katex.min.js')'
import katex from 'katex';
import html from './html';

export default {
	/*
	Render math formulas using katex.
	Necessary if dynamic formulas (with css class math) are added via JS after loading the page.
	Only new formulas are rendered (to save time)
	 */
	renderTex: function () {
		var elements=document.getElementsByClassName('math');
		for (var element of elements) {
			if (element.childNodes[0].className!='katex') {
				var texText=element.textContent;
				katex.render(texText, element, { throwOnError: false	});
			}
		}
	},

	/*
	 Detect formulas embedded in text (parsing vor $ signs used to enclose TeX-expressions) and prepare for rendering
	 */
	texit: function (text) {
		if (!text) text="";
		var parts=text.split('$');
		var inMathMode=false;
		var result="";

		for (var i=0; i<parts.length; i++) {
			var part=parts[i];
			//goto math mode, if text starts with $, meaning the firs part has the length 0
			if (part.length==0 && i==0) {
				inMathMode=true;
			}
			//add a $ sign to output (way to create real $ signs by escaping them as $$ in normal text)
			else if (part.length==0 && i+1<parts.length ) {
				result+="$";
				inMathMode=false;
			}
			//escape Text when outside math mode to prevent XSS attacks
			else if (!inMathMode) {
				result+=html.escapeHTML(part);
				inMathMode=true;
			}
			//insert Text into <span> Tags marking them as math formulas (to be rendered later by the renderTex function)
			else {
				result+="<span class='math'>"+html.escapeHTML(part)+"</span>";
				inMathMode=false;
			}
		}
		return result;
	},

	/*
	Render single TeX formular (in text) to Html using katex, currently not used, but still contained as this provides a core functionality.
	*/
	texToHtml: function(text, displayMode) {
		if (!text) return "";
		var html=katex.renderToString(text, {
	    	throwOnError: false,
			displayMode:displayMode
		});
		return html;
	}
}
