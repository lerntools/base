const MAIN_ROUTE='main-index';

export default {
	id: "main",
	meta: {
		title: 	{
			"de": "Lerntools",
			"en": "Learning Tools",
		},
		text:	{
			"de": "",
			"en": ""
		}
	},
	routes: [
		//basic navigation
		{	path: '/', redirect: { name: MAIN_ROUTE } },
		{	path: '/main-index', name:'main-index', component: () => import('./views/Home.vue') },
		{	path: '/main-admin', name: 'main-admin',  component: () => import('./views/Admin.vue')},
		{	path: '/main-error', name: 'main-error',  component: () => import('./views/Error.vue')},
		{	path: '/main-imprint', name: 'main-imprint',  component: () => import('./views/Imprint.vue')},
		{	path: '/main-privacy', name: 'main-privacy',  component: () => import('./views/Privacy.vue')},
		{	path: '/main-terms', name: 'main-terms',  component: () => import('./views/Terms.vue')},
		{ 	path: '/main-forbidden', name: 'main-forbidden', component: () => import('./views/Forbidden.vue')},
		{	path: '/main-version', name: 'main-version',  component: () => import('./views/Version.vue')},
		{	path: '/text-:module-:file-:lang', name: 'main-text-lang', component: () => import('./views/TextPage.vue')},
		{	path: '/text-:module-:file', name: 'main-text', component: () => import('./views/TextPage.vue')},
		//user management
		{	path: '/main-login', name:'main-login', component: () => import('./views/Login.vue') },
		{	path: '/main-logout', name:'main-logout', component: () => import('./views/Logout.vue') },
		{ 	path: '/main-profile', name: 'main-profile', component: () => import('./views/Profile.vue')},
		{ 	path: '/main-user-list', name: 'main-user-list', component: () => import('./views/UserList.vue')},
		{   path: '/main-reset-request', name: 'main-reset-request', component: () => import ('./views/ResetRequest.vue')},
		{   path: '/main-reset-confirm/:email/:ticket', name: 'main-reset-confirm', component: () => import ('./views/ResetConfirm.vue')},
		{ 	path: '/main-register-request', name:'main-register-request', component: () => import ('./views/RegisterRequest.vue')},
		{   path: '/main-register-confirm/:ticket', name: 'main-register-confirm', component: () => import ('./views/RegisterConfirm.vue')}
	]
}
