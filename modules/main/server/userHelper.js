exports.checkUserRole=function(res, role) {
	return role && res && res.locals && res.locals.user && res.locals.user.roles && res.locals.user.roles.indexOf(role)>=0;
}
