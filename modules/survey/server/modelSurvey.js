var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var SurveyPartSchema=require('./modelSurveyPartSchema.js');

var SurveySchema=new Schema({
	name :		{type: String, required: true, max: 100},
	desc :		{type: String, required: true, max: 300},
	owner:		{type: String, max: 100},
	visible:	{type: Boolean, default: false},
	parts:		[SurveyPartSchema]
});

module.exports=mongoose.model('Survey', SurveySchema );
