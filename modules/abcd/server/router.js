var express=require('express');
var router=express.Router();
var controller=require('./controller');
var mainController=require('../../main/server/controller');

//author
router.get('/sets-meta',controller.checkAuth,controller.getSetsMeta);
router.get('/sets/:id',controller.checkAuth,controller.getSet);
router.post('/sets',controller.checkAuth,controller.addNewSet);
router.put('/sets/:id',controller.checkAuth,controller.updateSet);
router.delete('/sets/:id',controller.checkAuth,controller.loadSetFromId, controller.deleteSet);

//teacher
router.post('/instances', controller.newInstance);
router.get('/instances/:key/participants',controller.checkAuth,controller.loadInstanceFromKey,controller.getParticipants);
router.delete('/instances/:key/participants/:partid', controller.checkAuth,controller.loadInstanceFromKey,controller.delParticipant);
router.post('/instances/:key/state', controller.checkAuth, controller.loadInstanceFromKey, controller.setState);
router.get('/instances/:key/votes',controller.checkAuth,controller.loadInstanceFromKey,controller.getVotes);

//participant
router.post('/part/login',controller.login);
router.post('/part/vote',controller.loadInstanceFromKey,controller.vote);
router.get('/part/sets/:ticket',controller.getSetByTicket);

//admin
router.get('/admin/sets-meta', mainController.checkAdmin,controller.getSetsMetaAdmin);
router.delete('/admin/sets/:id',mainController.checkAdmin,controller.loadSetFromId, controller.deleteSet);

module.exports=router;
