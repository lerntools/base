var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var AbcdInstanceSchema=new Schema({
	key: {type: String, required: true, unique: true},
	title: {type: String, required: true},
	owner:	{type: String, max: 100},
	state: {type: String, default: 'lobby'},
	currentQuestion: {type: Number, default: 0},
	participants: [{
		name: {type: String, required: true, max:100},
		points: {type: Number, default: 0},
		token: {type:String, max:100}
	}],
	votes: [{
		participant: {type: String, required: true},
		question: {type: Number, required:true},
		option: {type: Number, default: 0},
	}]
})

module.exports=mongoose.model('AbcdInstance', AbcdInstanceSchema );
