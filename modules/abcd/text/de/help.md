# ABCD-Quiz

Anleitung für Autoren{.subtitle}


Das „ABCD-Quiz“ ist eine Mischung aus der Publikumsfrage bei „Wer wird Millionär?“ und dem „Ampelquiz“. Ein Spiel besteht aus mehreren Fragen, zu denen es jeweils vier vorgegebene Antworten (A, B, C und D) gibt, von denen nur eine zutifft. Die Frage wird dabei gleichzeitig allen Teilnehmern präsentiert (am Beamer), wobei diese die Antwort in einer „geheimen Wahl“ über ihr Smartphone treffen. Der Zeitraum zum Beantworten einer Frage ist dabei eingeschränkt. Moderator und Teilnehmer benötigen keine spezielle Software, ein aktueller Browser ist ausreichend.

## Tipps zur Bedienung

- Anstelle des blauen Pfeils zum Weiterblättern kann auch die Leertaste genutzt werden.

## Starten eines Spiels

Nur eine auf der Webseite registrierte Person mit der entsprechenden Berechtigung, kann als ModeratorIn ein neues Spiel starten oder auch neue Fragen anlegen:

1. Anmelden an der Webseite (wichtig, sonst erscheint das ABCD-Quiz in der Teilnehmersicht)
2. Navigation zum Punkt ABCD-Quiz
3. Anklicken des Symbols zum Starten (Dreieck)

![](text/de/abcd/abcd-menu.png){.center}

Zur Verfügung stehen alle selbst erstellten Fragensammlungen sowie solche, die von anderen Autoren als öffentlich gekennzeichnet wurden. Um die Suche zur vereinfachen, kann in der Auswahlliste die Anzeige auf ein bestimmtes Thema / Fach beschränkt werden.

## Die Lobby

![](text/de/abcd/abcd-lobby.png){.center}

Nach dem Starten des ABCD-Quiz erscheint zunächst die Lobby, die einen QR-Code für die Teilnehmer enthält.
Anstelle des QR-Codes können auch die angezeigte URL sowie der Schlüssel verwendet werden.

![](text/de/abcd/abcd-teilnehmer.png){.center}

Zu Beginn wird jeder Teilnehmer aufgefordert, einen NickName zu vergeben (der nicht permanent in der Datenbank gespeichert wird). Dieser NickName erscheint anschließend im rechten Bereich der Lobby. Unpassende Namen können über das Mülleimersymbol aus dem Spiel verbannt werden. Bei den Teilnehmern wird nun eine schlichte Seite mit vier Buttons (A, B, C, D) angezeigt.

## Fragen & Antworten

Sind alle Teilnehmer im Spiel, beginnt der Moderator das Quiz mittels der Schaltfläche „Starten“ und es erscheinen im Wechsel nun Fragen und Antworten. Die Teilnehmer können dabei ihre Stimme nur abgeben, wenn eine Frage aktiv ist.

![](text/de/abcd/abcd-frage.png){.center}

Nach jeder Frage wird die korrekte Antwort aufgelöst und die Stimmverteilung angezeigt:

![](text/de/abcd/abcd-antwort.png){.center}

Am Ende des Spiels erscheint eine Übersicht der Teilnehmer, geordnet nach der erzielten Punktzahl (eine richtige Antwort zählt als ein Punkt).

![](text/de/abcd/abcd-ergebnis.png){.center}
