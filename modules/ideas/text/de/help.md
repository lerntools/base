# Ideensammlung

Anleitung für Autoren{.subtitle}

Eine „Ideensammlung“ dient dem gemeinsamen Brainstorming zu einem bestimmten Thema. Sie besteht aus einer Anzahl von Karten, die jeweils mehrere Inhalte aufnehmen können. Ein Inhalt kann ein Text, ein Bild oder auch ein Link sein.

![](text/de/ideas/beispiel1.png){.center}

An einer Ideensammlung können *gleichzeitig* mehrere Personen arbeiten, die Änderungen werden dabei automatisch und (fast) ohne Verzögerung synchronisiert. Dazu ist allerdings ein aktueller Webbrowser (z.B. Firefox, Safari, Chrome) mit Unterstützung für Websockets nötig.

## Autor

### Erstellen einer Ideensammlung

Nur ein eine auf der Webseite registrierte Person mit der entsprechenden Berechtigung, kann als *AutorIn* eine neue Ideensammlung erstellen:

1. Anmelden an der Webseite (wichtig, sonst erscheint die Ideensammlung in der Teilnehmersicht)
2. Navigation zum Punkt Ideensammlung
3. Anklicken des blauen Plus-Symbols

### Einstellungen einer Ideensammlung

![](text/de/ideas/beispiel3.png){.center}

- Titel: Kurze Bezeichnung der Ideensammlung, erscheint bei Teilnehmer als Titel der Seite
- Beschreibung: Kurze Erläuterung, erscheint als zweite Zeile im Titel
- Ticket: Zeichenfolge, die weiteren *Teilnehmern* den Zugriff auf die Ideensammlung ermöglicht
- Teilnehmer dürfen Inhalte...:

	- hinzufügen: Recht, neue Inhalte auf bestehende Karten zu schreiben
	- ändern: Recht, bestehende Inhalte auf einer Karte zu ändern
	- löschen: Recht, bestehende Inhalte zu löschen (auch für Verschieben nötig)

- Teilnehmer dürfen Karten...:

	- hinzufügen: Recht, neue Karten hinzuzufügen
	- umbenennen: Recht, den Titel (und die Farbe) einer bestehenden Karte zu ändern
	- löschen: Recht, eine bestehende Karte zu löschen

- Verschlüsselung der Inhalte: Client-seitige Verschlüsselung der Inhalte, nicht umkehrbar und nur möglich bei neuen Sammlungen.
- Information bei Änderungen: Optional erhält der Autor einmal pro Tag per E-Mail eine Liste der Änderungen
- Mathematische Formeln: Unterstützung des TeX-Syntax für Einträge, z.B. $f(x)=ax^2+bx+c$
- „Speichern“ übernimmt die Einstellungen, die neu erstellte Ideensammlung erscheint jetzt in der Übersicht.

### Hinweise zur Verschlüsselung

Wird die Option „Verschlüsselung der Inhalte“ gewählt, so werden Inhalte der Ideensammlung auf Seiten des Clients (d.h. im Browser) verschlüsselt.

- Die Verschlüsselung kann nur aktiviert werden, wenn die Ideensammlung noch keine Inhalte besitzt.
- Eine einmal aktivierte Verschlüsselung kann nicht rückgängig gemacht werden.
- Der erste Nutzer der Ideensammlung legt den Schlüssel fest, anschließend **kann der Schlüssel nicht mehr geändert werden**.
- Alle anderen Nutzer benötigen diesen gemeinsamen Schlüssel, der nicht auf dem selben Weg wie das Ticket übermittelt werden sollte!
- Auf technischer Ebene erfolgt die Verschlüsselung mittels AES265 (symmetrisch).
- Verschlüsselte Ideensammlungen können nicht exportiert oder dupliziert werden.
- Die Verschlüsselung wird durch das ⚷ Symbol visualisiert.

### Übersicht der Ideensammlungen

Die Ideensammlungen werden in der Übersicht tabellarisch dargestellt:

![](text/de/ideas/beispiel2.png){.center}

- Anzeigen (Dreieck): Anzeigen der Ideensammlung, wobei der Autor grundsätzlich alle Rechte besitzt
- Bearbeiten (Stiftsymbol): Ändern der Einstellungen der Ideensammlung, z.B. kann eine abgeschlossene Sammlung anschließend komplett gesperrt werden und als reine Informationstafel verwendet werden
- Teilen (Share): Anzeige des Tickets und eines QR-Codes für die Teilnehmer
- Export (Pfeil): Exportieren der Sammlung im Markdown bzw. HTML-Format. Möchten Sie die Sammlung z.B. mit LibreOffice weiterbearbeiten, ist dies über das HTML-Format gut möglich.
- Löschen (Mülleimer): Löschen der Ideensammlung mit allen Karten und Inhalten. Dieser Schritt kann nicht rückgängig gemacht werden.

### Tipps

- Als Autor können einzelne Karten gesperrt werden. Dies ist nach dem Anzeigen (siehe oben) einer Ideensammlung durch einen Klick auf den Kopf der Karte möglich.
- Auch das Verstecken von Karten ist möglich. Bitte beachten Sie, dass die Daten technisch versierten Benutzern dennoch zur Verfügung stehen, also lediglich die Anzeige unterdrückt wird.
- Die Reihenfolge der Karten richtet sich grundsätzlich nach ihrer Erstellung. Allerdings kann nachträglich eine Sortierung festgelegt werden (ebenfalls durch Klick auf den Kopf einer Karte)
- Inhalte können per Drag & Drop von einer Karte zur anderen verschoben werden (oder auch über das Kontextmenu)

##	Teilnehmer

### Beitreten zu einer Ideensammlung

Um an einer Ideensammlung teilzunehmen, stehen zwei Möglichkeiten zur Verfügung:

1. Nutzung des QR-Codes, den der Autor mit der Funktion Teilen anzeigt
2. Aufruf des Punkts Ideensammlung in der Navigation und Eingabe des Tickets

### Arbeiten mit der Ideensammlung

Die Möglichkeiten der Bearbeitung sind davon abhängig, welche Rechte der Autor definiert hat. Eine kurze Hilfe zur den möglichen Aktionen kann mit der Schaltfläche „Hilfe“ angezeigt werden.
