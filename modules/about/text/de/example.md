---
title: Über diese Seite
subtitle: Informationen und Hinweise zu dieser Lerntools-Instanz
---
# Hinweise

Passen Sie die Inhalte dieser Seite an und speichern Sie sie unter der Bezeichnung `index.md` in diesem Verzeichnis.

Sie können auch weitere Markdown-Dateien anlegen und diese mittels der URL `#/text-about-NAME` aus der Datei `index.md` verlinken. Dabei entspricht NAME dem Namen der weiteren Markdown-Datei, der keinen Bindestrich (-) enthalten darf: [Beispiellink](#/text-about-example)

Um auch Unterstützung für Englische Sprache zu erhalten, müssen Sie den Vorgang im Unterverzeichnis text/en/ wiederholen.
