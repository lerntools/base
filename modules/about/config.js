
import Icon from './img/about.svg';

export default {
	id: "about",
	meta: {
		title: 	{
			"de": "Über diese Seite",
			"en": "About this page"
		},
		text:	{
			"de": "Informationen und Hinweise zu dieser Lerntools-Instanz",
			"en": "Information and notes on this learning tools instance"
		},
		to: 	"about-index",
		icon: 	Icon,
		index:	true,
	},
	routes: [
		{	path: '/about-index', name:'about-index', component: () => import('./views/Index.vue') },
	]
}
