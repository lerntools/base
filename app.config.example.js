/*********************************************************
 * Configure app - change according to your setup
 *********************************************************/

//load main module - don't remove
import modMain from './modules/main/config';

//load further modules - remove if you don't need them
import modAbcd from './modules/abcd/config';
import modAbout from './modules/about/config';
import modIdeas from './modules/ideas/config';
import modSurvey from './modules/survey/config';

//basic settings
export default {
	author:'Lerntools-Team',	//author name
	pageTitle:'Lerntools',		//html head page title
	urlBackend:'/',				//backend api, leave as '/' when running your own backend
	urlImprint:'',				//url of external imprint page
	urlTerms:'',				//url of external terms page
	urlPrivacy:'',				//url of external privacy page
	urlHome:'',					//url of external link from home icon
	defaultLocale:'de',			//default locale
	fixNavbar: true,			//always display top navbar at fixed position
	allModules: [
		modMain, 				//don't remove main module
		modAbout, modAbcd, modIdeas, modSurvey
	]
}
