FROM ubuntu:22.04

RUN apt-get update -y && \
        DEBIAN_FRONTEND=noninteractive \
        apt-get upgrade -y && \
        DEBIAN_FRONTEND=noninteractive \
        apt-get install -y curl ca-certificates gnupg --no-install-recommends && \
        # installing nodejs and npm according to https://github.com/nodesource/distributions#debian-versions
        mkdir -p /etc/apt/keyrings && \
        curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg && \
        echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list && \
        apt-get -y update && \
        DEBIAN_FRONTEND=noninteractive \
        apt-get install -y nodejs git autoconf libcurl4 libgl1 libxi6 nano texlive-latex-base --no-install-recommends && \
        rm -rf /var/lib/apt/lists/* && \
        npm install -g npm && \
        # add user
        groupadd -r user && \
        useradd -d /srv -r -g user user && \
        chown user:user /srv

USER user

# Hotfix until Webpack 5 is used
ENV NODE_OPTIONS="--openssl-legacy-provider"

COPY --chown=user:user . /srv/base/

RUN cp /srv/base/app.config.example.js /srv/base/app.config.js && \
        mkdir -p /srv/base/upload && \
        mkdir -p /srv/base/data && \
        mkdir -p /srv/base/modules && \
        cd /srv/base/modules && \
        git clone https://codeberg.org/lerntools/abcd.git && \
        git clone https://codeberg.org/lerntools/about.git && \
        git clone https://codeberg.org/lerntools/ideas.git && \
        git clone https://codeberg.org/lerntools/projector.git && \
        git clone https://codeberg.org/lerntools/survey.git && \
        git clone https://codeberg.org/lerntools/tex.git && \
        cd /srv/base && \
        npm update && \
        npm install && \
        npm run build

WORKDIR /srv/base

ENTRYPOINT ["bin/docker-entrypoint.sh"]
